
import {Card, Button, Container, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';



export default function ViewAllProduct({viewAllProp}) {

	
	const { productImage, productName, description, price, _id, isActive} = viewAllProp;

    return (
        <Container className="container-fluid p-3"> 
            <Col lg={{span:8, offset: 2}} className="d-flex justify-content-center text-center">
                <Card>
                    <Card.Body>
                        <img className="img-fluid" width="300" alt="product" src={productImage}/>
                        <Card.Title className="p-2">{productName}</Card.Title>
                        <Card.Text>ID: {_id}</Card.Text>
                        <Card.Text>Is Product Active?: {String(isActive)}</Card.Text>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>Php {price}</Card.Text>
                        <Button id="admin-btn" as={Link} to={`/products/update/${_id}`}>Update</Button>
                        <Button id="admin-btn" as={Link} to={`/products/${_id}/archive`}>Deactivate</Button>
                        <Button id="admin-btn" as={Link} to={`/products/${_id}/reactivate`}>Reactivate</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Container>
    )
}









