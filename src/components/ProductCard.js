
import {Card, Button, Col, Container, Row} from 'react-bootstrap';
import {Link} from 'react-router-dom';



export default function ProductCard({productProp}) {

	
	const { productImage, productName, description, price, _id} = productProp;

    return (
    <Row className=" pb-3 container-fluid">
    <Container className="d-flex justify-content-center text-center">
        
       
            <Card >
                <Card.Body>
                    <img id="img-box" width={250} height={350} className="img-fluid" alt="product" src={productImage}/>
                    <Card.Title className="p-2">{productName}</Card.Title>
                    <Card.Subtitle className="p-1">{description}</Card.Subtitle>
                    <Card.Text>Php {price}</Card.Text>
                    <Button id="admin-btn" variant="primary" as={Link} to={`/products/${_id}`}>Details</Button>
                </Card.Body>
            </Card>
        
  

    </Container>
    </Row>
    )
}



/*

Alternative:

 <Link className="btn btn-primary" to="/courseView">Details</Link>


*/





