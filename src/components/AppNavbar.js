
import { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { MDBIcon } from 'mdb-react-ui-kit';
import UserContext from '../UserContext';

export default function AppNavbar() {

	const {user} = useContext(UserContext);

	return(

		(user.isAdmin !== true ) ?

		<Navbar collapseOnSelect expand="lg" id="navbar-bg" variant="light">
		       <Container>
		       	 <MDBIcon id="nav-logo" fas icon="glass-cheers" />
		         <Navbar.Brand id="nav-icon" className="m-auto" href="#home">Liq·uor
		         </Navbar.Brand>
		        	 <Navbar.Toggle aria-controls="responsive-navbar-nav" />
			         <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
				        
				         	         <Nav>
				         	          <Nav.Link id="nav-text" as={Link} to="/">
				         	          <MDBIcon fas icon="home" /> Home</Nav.Link>
				         	          <Nav.Link id="nav-text" as={Link} to="/Products">Products</Nav.Link>
					         	      </Nav>
					         	      <Nav className="justify-content-end">
				         	        {

				         	        	(user.id !== null) ?
				         	        		<>
				         	        		<Nav.Link id="nav-text" as={Link} to="/logout">Logout</Nav.Link>
				         	        		<Nav.Link id="nav-text" as={Link} to="/users/mycart/">
				         	        		<MDBIcon fas icon="shopping-cart" /></Nav.Link>
				         	        		<Nav.Link id="nav-text" as={Link} to="/users">
				         	        		<MDBIcon fas icon="user" /></Nav.Link>
				         	        		</>
				         	        		:
				         	        		<>
				         		        	<Nav.Link id="nav-text" as={Link} to="/login">Login</Nav.Link>
				         		       		<Nav.Link id="nav-text" as={Link} to="/register">Register</Nav.Link>
				         		       		</>
				         	        }
				         	         </Nav>

			         </Navbar.Collapse>
		       </Container>
		     </Navbar>


		    :

		    <Navbar collapseOnSelect expand="lg" id="navbar-bg" variant="light">
		       <Container>
		       	 <MDBIcon className="p-1" id="nav-logo" fas icon="cocktail" />
		         <Navbar.Brand id="nav-icon" className="m-auto" href="#home">Liq·uor
		         </Navbar.Brand>
		        	 <Navbar.Toggle aria-controls="responsive-navbar-nav" />
			         <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
				        
				         	         <Nav>
				         	          <Nav.Link id="nav-text" as={Link} to="/admin">
				         	          <MDBIcon fas icon="home" />Home</Nav.Link>
				         	          <Nav.Link id="nav-text" as={Link} to="/allproducts">Products</Nav.Link>
				         	          <Nav.Link id="nav-text" as={Link} to="/allusers">Users</Nav.Link>
				         	          <Nav.Link id="nav-text" as={Link} to="/createproducts">Create</Nav.Link>
				       
					         	      </Nav>
					         	      <Nav className="justify-content-end">
				         	        {

				         	        	(user.id !== null) ?
				         	        		<>
				         	        		<Nav.Link id="nav-text" as={Link} to="/logout">Logout</Nav.Link>
				         	        		
				         	        		</>
				         	        		:
				         	        		<>
				         		        	<Nav.Link id="nav-text" as={Link} to="/login">Login</Nav.Link>
				         		       		<Nav.Link id="nav-text" as={Link} to="/register">Register</Nav.Link>
				         		       		</>
				         	        }
				         	         </Nav>

			         </Navbar.Collapse>
		       </Container>
		     </Navbar>




		  )
}
