
import {Fragment, useEffect, useState, useContext} from 'react';
import UsersCard from '../components/UsersCard';
import UserContext from '../UserContext';

export default function AllUsers() {

	const { user } = useContext(UserContext);

	const [allUsers, setAllUsers] = useState([])

	useEffect(() => {
		

		fetch('https://fathomless-spire-69300.herokuapp.com/users/alldetails', {
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllUsers(data.map(allUsers => {

				return(
				<>
					<UsersCard key={allUsers._id} usersProp={allUsers} />

				</>
					)


			}))

		})

	}, [])


	return(

		<Fragment> 
			<h2>Show all registered users</h2>
			{allUsers}
		</Fragment>



		)
}

