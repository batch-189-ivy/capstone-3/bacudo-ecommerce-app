import {useState, useEffect, useContext} from 'react';
import {Button, Col, Row, Form, Container} from 'react-bootstrap';
import {useParams} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ReactivateProduct() {

	const { user } = useContext(UserContext);

	const{productId} = useParams()

	const [isActive, setIsActive] = useState("");
	

	const reactivate = (e) => {

		fetch(`https://fathomless-spire-69300.herokuapp.com/products/${productId}/reactivate`, {
			method: "PUT",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
					
					isActive: isActive

				})
		})
		.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data === true) {

						Swal.fire({
								title: "Updated Successfully",
								icon: "success",
								text: "You may now log in."
							})


				} else {
						Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again"
							})
				}

			})


	}


	useEffect(() => {
		

		fetch(`https://fathomless-spire-69300.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setIsActive(data.isActive)
			


		})

	}, [productId])



	return(

		<Container className="justify-content-center">
			  <Form onSubmit={(e) => reactivate(e)}>
			    <Row className="g-2 mt-3">
			      <Col className="text-center">
			      <h3 className="text-center p-2">Reactivate A Product</h3>

			      
			      	
			      	  <span>Reactivate this product?</span>
			          <Button id="admin-btn" type="submit" >
		   				  Update
		   				</Button>


			      </Col>
			     </Row>
			 </Form>
		  </Container>



		)






}


