
import {Fragment, useEffect, useState, useContext} from 'react';
import {Container, Button, Form} from 'react-bootstrap';
import {Link, useNavigate} from 'react-router-dom';
import CartCard from '../components/CartCard';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Cart() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [mycart, setMyCart] = useState([])


	function checkOutCart(e) {
			
		 	fetch('https://fathomless-spire-69300.herokuapp.com/users/mycart/checkout', {
		 	method: "POST",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)


			if (data === true) {
				Swal.fire({
					title: "Thanks for your order!",
					icon: "success",
					text: "Your order will be processed shortly"
					
				})

				navigate("/");

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again!"
				})
			}
			
		})
	}


	const removeItems = () => {

	         fetch("https://fathomless-spire-69300.herokuapp.com/users/removeItems/", {
	         method:"DELETE",
	         headers: {
	             "Content-type": "application/json",
	             Authorization: `Bearer ${localStorage.getItem('token')}`
	         }

	     })
	     .then(res => res.json())
	     .then(data => {
	         console.log(data)

	         if (data === true) {
	             Swal.fire({
	                 title: "Cart has been cleared!",
	                 icon: "success"
	                 
	             })

	             navigate("/products")

	         } else {
	             Swal.fire({
	                 title: "Something went wrong",
	                 icon: "error",
	                 text: "Please try again!"
	             })
	         }

	     })
	 }


	

	useEffect(() => {
		

		fetch(`https://fathomless-spire-69300.herokuapp.com/users/mycart`, {
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setMyCart(data.map(mycart => {

				return(
				<>
					<CartCard key={mycart._id} cartProp={mycart} />
					

				</>
					)


			}))

		})

	}, [])


	return(

		<Fragment>
		
		<Container className="p-3 d-inline-block justify-content-center text-center"> 
			<h2 className="p-1">My shopping cart</h2>
			<Button className="p-1 m-5" onClick={() => checkOutCart()}  id="admin-btn" >Checkout</Button>
			<Button className="p-1 m-5" onClick={() => removeItems()}  id="admin-btn" >Clear All</Button>
			<div>
				<div>
				{mycart}
				</div>
			</div>
		</Container>
		
		</Fragment>



		)
}

