

import {Fragment, useEffect, useState} from 'react';
import {Container, Row, Col, Pagination} from 'react-bootstrap';
import ProductCard from '../components/ProductCard';



export default function Products() {

	


	const[products, setProducts] = useState([])

	useEffect(() => {
		fetch('https://fathomless-spire-69300.herokuapp.com/products/')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {

				return(
					
					<ProductCard key={product._id} productProp={product} />

					)
			}))
		})


	}, [])

	




	return(
		
		<Container className="container-fluid">
			<h2 className="text-center p-3">Products</h2>
		<div className="row">
		<div className="col-xs">
		<div className="box">
		<div className="col-xs">
			<div style={{ width: '100%', height: '100%;' }} id="admin-view">{products}</div>
		</div>
		</div>
		</div>
		</div>

		</Container>
		

		  
		
		
		
		)


}